package com.ppm.model;

import java.io.Serializable;

/**
 * 
 * @author Pedro T. Oliveira <pedro.oliveira.nom.br>
 */
public abstract class ValueObject implements Serializable {

	/**
	 * The Serial Version UID
	 */
	private static final long serialVersionUID = -6936755796952687364L;
}
