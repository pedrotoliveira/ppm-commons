package com.ppm.logging;

/**
 * Severity types for logging.
 * 
 * @author Pedro T. Oliveira <pedro.oliveira20@gmail.com>
 */
public enum Severity {
    INFO, WARN, DEBUG, ERROR, TRACE, FATAL;
}
